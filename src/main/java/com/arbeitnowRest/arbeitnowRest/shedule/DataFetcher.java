package com.arbeitnowRest.arbeitnowRest.shedule;

import com.arbeitnowRest.arbeitnowRest.convertor.JobConverter;
import com.arbeitnowRest.arbeitnowRest.dto.DataDTO;
import com.arbeitnowRest.arbeitnowRest.dto.JobListing;
import com.arbeitnowRest.arbeitnowRest.repository.JobRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
public class DataFetcher {

    private final JobRepository jobRepository;
    private final RestTemplate restTemplate;


    @Value("${url.job.board.api}")
    private String url;

    @Scheduled(fixedDelayString = "${schedule.delay}")
    public void fetchDataAndStoreInDB() {
        List<DataDTO> data = IntStream.range(1, 6)
                .boxed()
                .map(pageNum -> restTemplate.getForObject(url + "?page=" + pageNum, JobListing.class))
                .filter(Objects::nonNull)
                .map(JobListing::getData)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .toList();
        jobRepository.deleteAll();
        data.forEach(u -> jobRepository.save(JobConverter.dataDTOConvertToJob(u))
        );

    }

}
