package com.arbeitnowRest.arbeitnowRest.convertor;

import com.arbeitnowRest.arbeitnowRest.dto.DataDTO;
import com.arbeitnowRest.arbeitnowRest.dto.JobDTO;
import com.arbeitnowRest.arbeitnowRest.dto.StatisticDTO;
import com.arbeitnowRest.arbeitnowRest.entity.Job;
import com.arbeitnowRest.arbeitnowRest.exception.JobsListIsEmptyException;
import com.arbeitnowRest.arbeitnowRest.repository.JobRepository;

import java.util.List;

public class JobConverter {

    public static Job dataDTOConvertToJob(DataDTO data) {
        return Job.builder()
                .company(data.getCompany_name())
                .title(data.getTitle())
                .url(data.getUrl())
                .remote(data.getRemote())
                .location(data.getLocation())
                .build();

    }

    public static List<JobDTO> jobConvertToJobDTO(List<Job> jobs) {
        if (jobs == null)
            throw new JobsListIsEmptyException("Job list is empty!");

        return jobs.stream()
                .map(job ->
                        JobDTO.builder()
                                .id(job.getId())
                                .company(job.getCompany())
                                .title(job.getTitle())
                                .url(job.getUrl())
                                .remote(job.getRemote())
                                .location(job.getLocation())
                                .build()
                )
                .toList();

    }

    public static List<StatisticDTO> countCitiesToStatisticDTO(List<JobRepository.MyCount> myCounts) {
        if (myCounts == null)
            throw new JobsListIsEmptyException("Statistic is empty!");

        return myCounts.stream()
                .map(st ->
                        StatisticDTO.builder()
                                .location(st.getLocation())
                                .count(st.getLocationCount())
                                .build()
                )
                .toList();
    }
}
