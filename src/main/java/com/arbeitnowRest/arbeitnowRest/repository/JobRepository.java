package com.arbeitnowRest.arbeitnowRest.repository;

import com.arbeitnowRest.arbeitnowRest.entity.Job;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobRepository extends CrudRepository<Job, Long> {

    List<Job> findAll(Pageable pageable);

    @Query(value = "SELECT location, COUNT(id) AS locationCount FROM jobs GROUP BY location", nativeQuery = true)
    List<MyCount> calculateVacancyByCities();

    interface MyCount {
        String getLocation();

        Long getLocationCount();

    }

}
