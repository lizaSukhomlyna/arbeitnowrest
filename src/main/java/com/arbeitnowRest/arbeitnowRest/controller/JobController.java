package com.arbeitnowRest.arbeitnowRest.controller;

import com.arbeitnowRest.arbeitnowRest.convertor.JobConverter;
import com.arbeitnowRest.arbeitnowRest.dto.JobDTO;
import com.arbeitnowRest.arbeitnowRest.dto.StatisticDTO;
import com.arbeitnowRest.arbeitnowRest.service.JobService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/job")
@RequiredArgsConstructor
public class JobController {

    private final JobService jobService;

    @GetMapping("/getPageOfWithSortById")
    public List<JobDTO> getPageOfJobsWithSortById(@RequestParam(required = false, defaultValue = "0") Integer numPage) {
        return JobConverter.jobConvertToJobDTO(jobService.getPageOfJobsWithSortById(numPage));
    }

    @GetMapping("/getPageOfWithSortByCompanyName")
    public List<JobDTO> getPageOfJobsWithSortByCompanyName(@RequestParam(required = false, defaultValue = "0") Integer numPage) {
        return JobConverter.jobConvertToJobDTO(jobService.getPageOfJobsWithSortByCompanyName(numPage));
    }

    @GetMapping("/getPageOfWithSortByTitle")
    public List<JobDTO> getPageOfJobsWithSortByTitle(@RequestParam(required = false, defaultValue = "0") Integer numPage) {
        return JobConverter.jobConvertToJobDTO(jobService.getPageOfJobsWithSortByTitle(numPage));
    }

    @GetMapping("/getPageOfWithSortByLocation")
    public List<JobDTO> getPageOfJobsWithSortByLocation(@RequestParam(required = false, defaultValue = "0") Integer numPage) {
        return JobConverter.jobConvertToJobDTO(jobService.getPageOfJobsWithSortByLocation(numPage));
    }

    @GetMapping("/getCountVacanciesByLocation")
    public List<StatisticDTO> getCountVacanciesByLocation() {
        return JobConverter.countCitiesToStatisticDTO(jobService.getCountVacancyByLocation());
    }
}
