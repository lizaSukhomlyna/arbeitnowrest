package com.arbeitnowRest.arbeitnowRest.exception;

public class JobsListIsEmptyException extends RuntimeException {
    String message;

    public JobsListIsEmptyException(String message) {
        this.message = message;
    }
}
