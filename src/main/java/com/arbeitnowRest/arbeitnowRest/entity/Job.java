package com.arbeitnowRest.arbeitnowRest.entity;


import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.Accessors;

@Getter
@Setter
@Entity
@Table(name = "jobs")
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "company")
    private String company;

    @Column(name = "title")
    private String title;


    @Column(name = "remote")
    private Boolean remote;

    @Column(name = "url")
    private String url;

    @Column(name = "location")
    private String location;


}
