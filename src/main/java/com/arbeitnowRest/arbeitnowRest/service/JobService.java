package com.arbeitnowRest.arbeitnowRest.service;

import com.arbeitnowRest.arbeitnowRest.entity.Job;
import com.arbeitnowRest.arbeitnowRest.repository.JobRepository;

import java.util.List;

public interface JobService {
    List<Job> getPageOfJobsWithSortById(Integer numPage);

    List<Job> getPageOfJobsWithSortByCompanyName(Integer numPage);

    List<Job> getPageOfJobsWithSortByTitle(Integer numPage);

    List<Job> getPageOfJobsWithSortByLocation(Integer numPage);

    List<JobRepository.MyCount> getCountVacancyByLocation();


}
