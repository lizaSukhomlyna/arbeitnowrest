package com.arbeitnowRest.arbeitnowRest.service;

import com.arbeitnowRest.arbeitnowRest.entity.Job;
import com.arbeitnowRest.arbeitnowRest.repository.JobRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class JobServiceImpl implements JobService {

    private final JobRepository jobRepository;

    final Integer amountRecords = 10;


    @Override
    public List<Job> getPageOfJobsWithSortById(Integer numPage) {
        Pageable pag = PageRequest.of(numPage, amountRecords, Sort.by("id"));

        return jobRepository.findAll(pag);
    }

    @Override
    public List<Job> getPageOfJobsWithSortByCompanyName(Integer numPage) {
        Pageable pag = PageRequest.of(numPage, amountRecords, Sort.by("company"));
        return jobRepository.findAll(pag);
    }


    @Override
    public List<Job> getPageOfJobsWithSortByTitle(Integer numPage) {
        Pageable pag = PageRequest.of(numPage, amountRecords, Sort.by("title"));
        return jobRepository.findAll(pag);
    }


    @Override
    public List<Job> getPageOfJobsWithSortByLocation(Integer numPage) {
        Pageable pag = PageRequest.of(numPage, amountRecords, Sort.by("location"));
        return jobRepository.findAll(pag);
    }

    @Override
    public List<JobRepository.MyCount> getCountVacancyByLocation() {
        return jobRepository.calculateVacancyByCities();
    }


}
