package com.arbeitnowRest.arbeitnowRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ArbeitnowRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArbeitnowRestApplication.class, args);
    }

}
