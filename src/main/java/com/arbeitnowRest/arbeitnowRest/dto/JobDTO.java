package com.arbeitnowRest.arbeitnowRest.dto;

import lombok.*;

@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class JobDTO {
    private Long id;
    private String company;
    private String title;
    private Boolean remote;
    private String url;
    private String location;
}
