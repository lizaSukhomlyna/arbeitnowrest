package com.arbeitnowRest.arbeitnowRest.dto;


import lombok.*;

@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class StatisticDTO {
    String location;

    Long count;
}
