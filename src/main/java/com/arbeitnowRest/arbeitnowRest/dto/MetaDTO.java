package com.arbeitnowRest.arbeitnowRest.dto;

import lombok.Data;

@Data
public class MetaDTO {
    private String current_page;
    private Integer from;
    private String path;
    private Integer per_page;
    private Integer to;
    private String terms;
    private String info;
}
