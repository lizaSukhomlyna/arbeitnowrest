package com.arbeitnowRest.arbeitnowRest.dto;

import lombok.Data;

@Data
public class LinkDTO {
    private String first;
    private String last;
    private String prev;
    private String next;
}
