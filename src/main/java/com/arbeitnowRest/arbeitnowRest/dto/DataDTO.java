package com.arbeitnowRest.arbeitnowRest.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DataDTO {
    private String slug;
    private String company_name;
    private String title;
    private String description;
    private Boolean remote;
    private String url;
    private String[] tags;
    private String[] job_types;
    private String location;
    private Integer created_at;
}
