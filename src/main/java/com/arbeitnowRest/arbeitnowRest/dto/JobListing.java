package com.arbeitnowRest.arbeitnowRest.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class JobListing {
    private List<DataDTO> data;
    private LinkDTO links;
    private MetaDTO meta;

}
