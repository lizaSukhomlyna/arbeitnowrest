package com.arbeitnowRest.arbeitnowRest.service;

import com.arbeitnowRest.arbeitnowRest.entity.Job;
import com.arbeitnowRest.arbeitnowRest.repository.JobRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class JobServiceImplTest {


    @Mock
    JobRepository jobRepository;

    @InjectMocks
    JobServiceImpl jobService;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        List<Job> jobs = new ArrayList<>();
        jobs.add(new Job()
                .setUrl("https://www.arbeitnow.com/jobs/companies/alten/software-engineer-cyber-security-all-gender-friedrichshafen-36463")
                .setCompany("ALTEN")
                .setTitle("Software Engineer Cyber Security (all gender)")
                .setRemote(true)
                .setLocation("Wolfsburg"));
        jobs.add(new Job()
                .setUrl("https://www.arbeitnow.com/jobs/companies/alten/software-engineer-cyber-security-all-gender-friedrichshafen-36463")
                .setCompany("ALTEN")
                .setTitle("(Junior) Softwareentwickler Fahrerassistenzsysteme (all gender)")
                .setRemote(false)
                .setLocation("Friedrichshafen"));
        jobs.add(new Job()
                .setUrl("https://www.arbeitnow.com/jobs/companies/alten/software-engineer-cyber-security-all-gender-friedrichshafen-36463")
                .setCompany("Bosch Group")
                .setTitle("(Junior) Softwareentwickler Fahrerassistenzsysteme (all gender)")
                .setRemote(false)
                .setLocation("Friedrichshafen"));

        when(jobRepository.findAll(any(Pageable.class))).thenReturn(jobs);
    }

    @Test
    void getPageOfJobsWithSortById() {
        int pageNum = 0;
        Pageable pag = PageRequest.of(pageNum, 10, Sort.by("id"));
        jobService.getPageOfJobsWithSortById(pageNum);
        verify(jobRepository, times(1)).findAll(pag);
    }

    @Test
    void getPageOfJobsWithSortByCompanyName() {
        int pageNum = 0;
        Pageable pag = PageRequest.of(pageNum, 10, Sort.by("company"));
        jobService.getPageOfJobsWithSortByCompanyName(pageNum);
        verify(jobRepository, times(1)).findAll(pag);
    }

    @Test
    void getPageOfJobsWithSortByTitle() {
        int pageNum = 0;
        Pageable pag = PageRequest.of(pageNum, 10, Sort.by("title"));
        jobService.getPageOfJobsWithSortByTitle(pageNum);
        verify(jobRepository, times(1)).findAll(pag);
    }

    @Test
    void getPageOfJobsWithSortByLocation() {
        int pageNum = 0;
        Pageable pag = PageRequest.of(pageNum, 10, Sort.by("location"));
        jobService.getPageOfJobsWithSortByLocation(pageNum);
        verify(jobRepository, times(1)).findAll(pag);
    }

}