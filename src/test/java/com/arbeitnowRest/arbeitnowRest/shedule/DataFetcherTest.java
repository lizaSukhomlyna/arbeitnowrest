package com.arbeitnowRest.arbeitnowRest.shedule;

import com.arbeitnowRest.arbeitnowRest.dto.DataDTO;
import com.arbeitnowRest.arbeitnowRest.dto.JobListing;
import com.arbeitnowRest.arbeitnowRest.entity.Job;
import com.arbeitnowRest.arbeitnowRest.repository.JobRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static org.mockito.Mockito.*;

class DataFetcherTest {

    @Mock
    private JobRepository jobRepository;
    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private DataFetcher dataFetcher;


    @BeforeEach
    public void setUp() {
        JobListing jobListing1 = new JobListing().setData(Collections.singletonList(new DataDTO()
                .setCompany_name("Company1")
                .setDescription("DesciptionOfPosition1")
                .setLocation("Dnipro1")));
        JobListing jobListing2 = new JobListing().setData(Collections.singletonList(new DataDTO()
                .setCompany_name("Company2")
                .setDescription("DesciptionOfPosition2")
                .setLocation("Dnipro2")));
        JobListing jobListing3 = new JobListing().setData(Collections.singletonList(new DataDTO()
                .setCompany_name("Company3")
                .setDescription("DesciptionOfPosition3")
                .setLocation("Dnipro3")));
        JobListing jobListing4 = new JobListing().setData(Collections.singletonList(new DataDTO()
                .setCompany_name("Company4")
                .setDescription("DesciptionOfPosition4")
                .setLocation("Dnipro4")));
        JobListing jobListing5 = new JobListing().setData(Collections.singletonList(new DataDTO()
                .setCompany_name("Company5")
                .setDescription("DesciptionOfPosition5")
                .setLocation("Dnipro5")));
        JobListing jobListing6 = new JobListing().setData(Collections.singletonList(new DataDTO()
                .setCompany_name("Company6")
                .setDescription("DesciptionOfPosition6")
                .setLocation("Dnipro6")));

        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(dataFetcher, "url", "http://example.com/jobs");
        when(restTemplate.getForObject("http://example.com/jobs?page=0", JobListing.class))
                .thenReturn(jobListing1);
        when(restTemplate.getForObject("http://example.com/jobs?page=1", JobListing.class))
                .thenReturn(jobListing2);
        when(restTemplate.getForObject("http://example.com/jobs?page=2", JobListing.class))
                .thenReturn(jobListing3);
        when(restTemplate.getForObject("http://example.com/jobs?page=3", JobListing.class))
                .thenReturn(jobListing4);
        when(restTemplate.getForObject("http://example.com/jobs?page=4", JobListing.class))
                .thenReturn(jobListing5);
        when(restTemplate.getForObject("http://example.com/jobs?page=5", JobListing.class))
                .thenReturn(jobListing6);

    }
    @Test
    void fetchDataAndStoreInDB() {
        dataFetcher.fetchDataAndStoreInDB();
        verify(jobRepository, times(1)).deleteAll();
        verify(jobRepository, times(5)).save(any(Job.class));

    }
}