# arbeitnowRest

java BE downloads 5 pages from https://www.arbeitnow.com/api/job-board-ap", stores in database (H2),
updates them periodically.

## REST API application has functionality:
* getting all vacancies with pagination and various sortings
* obtaining statistics on locations (city and number of vacancies)


**Stack:**
Java17, Spring, Junit, Mockito, lombock, h2
